import os
import unittest
#Package imports
from flask import current_app
from flask_testing import TestCase

#App imports
from manage import app
from app.main.config import basedir


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        app.config.from_object('app.main.config.DevelopmentConfig')
        return app

    def test_app_in_development(self):
        self.assertFalse(app.config['SECRET_KEY'] =='my_precious_key')
        self.assertTrue(app.config['DEBUG'] is True)
        self.assertFalse(current_app == None)


class TestTestingConfig(TestCase):
    def create_app(self):
        app.config.from_object('app.main.config.TestingConfig')
        return app

    def test_app_in_testing(self):
        self.assertFalse(app.config['SECRET_KEY'] == 'my_precious_key')
        self.assertTrue(app.config['DEBUG'])

class TestProductionConfig(TestCase):
    def create_app(self):
        app.config.from_object('app.main.config.ProductionConfig')
        return app

    def test_app_in_production(self):
        self.assertFalse(app.config['SECRET_KEY'] == 'my_precious_key')
        self.assertTrue(app.config['DEBUG'] is False)


if __name__ == '__main__':
    unittest.main()