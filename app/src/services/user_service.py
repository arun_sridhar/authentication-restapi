import datetime

from app.main import db
from app.main.model.user import User

def add_new_user(data):
    #check if user already exists
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        #extrace the data
        new_user = User(
            email=data['email'],
            password=data['password'],
            registered_on=datetime.datetime.utcnow()
        )
        save_changes(new_user)
        response_object = {
            'status': 'success',
            'message': 'Successfully registered.'
        }
        return response_object, 201
    response_object = {
        'status': 'fail',
        'message': 'User already exists. Please Log in.',
        }
    return response_object, 409

def get_user(id):
    return User.query.filter_by(id=id)
    

def save_changes(data):
    db.session.add(data)
    db.session.commit()